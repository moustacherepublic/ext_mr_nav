<?php
$this->startSetup();
//put price attribute to the end position so that the count can be correct
$this->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'price', 'position', 99999999);
$this->endSetup();
