<?php
class MR_Nav_Block_Catalog_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar {
    public function getTotalNum()
    {
        $this->getCollection()->getSelect()->reset(Zend_Db_Select::GROUP);
        $sql = $this->getCollection()->getSelectCountSql();

        return intval($this->getCollection()->getConnection()->fetchOne($sql));
    }
}