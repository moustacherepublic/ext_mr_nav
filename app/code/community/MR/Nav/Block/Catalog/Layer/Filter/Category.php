<?php
class MR_Nav_Block_Catalog_Layer_Filter_Category extends Mage_Catalog_Block_Layer_Filter_Category {
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('mr/mr_nav/filter.phtml');
    }
}