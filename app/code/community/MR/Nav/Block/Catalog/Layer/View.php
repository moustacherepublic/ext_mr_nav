<?php
class MR_Nav_Block_Catalog_Layer_View extends Mage_Catalog_Block_Layer_View
{
    public function getFilters()
    {
        $filters = parent::getFilters();
        foreach($filters as $f){
            if($f instanceof Mage_Catalog_Block_Layer_Filter_Price){
                $f->setTemplate('mr/mr_nav/price.phtml');
            }
        }
        return $filters;
    }
}

?>
