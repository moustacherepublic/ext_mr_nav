<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 28/10/14
 * Time: 7:51 PM
 */ 
class MR_Nav_Helper_Data extends Mage_Core_Helper_Abstract {
    public function getRequestValues($key)
    {
        $v = Mage::app()->getRequest()->getParam($key);

        if (is_array($v)){//smth goes wrong
            return array();
        }

        if (preg_match('/^[0-9,\-]+$/', $v)){
            $v = array_unique(explode(',', $v));
        }
        else {
            $v = array();
        }

        return $v;
    }

    public function findProductList($layout)
    {
        $page = $layout->getBlock('product_list');

        if (!$page){
            $page = $layout->getBlock('search_result_list');
        }

        if (!$page) {
            $page = $layout->getBlock('catalogsearch_advanced_result');
        }

        return $page;
    }

    public function findLayerView($layout)
    {
        $page = $layout->getBlock('catalog.leftnav');

        if (!$page){
            $page = $layout->getBlock('catalogsearch.leftnav');
        }

        return $page;
    }
}