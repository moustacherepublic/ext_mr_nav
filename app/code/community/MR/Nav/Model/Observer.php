<?php
class MR_Nav_Model_Observer
{
    public function handleLayoutRender()
    {
        $layout = Mage::getSingleton('core/layout');
        if (!$layout)
            return;

        $isAJAX = Mage::app()->getRequest()->getParam('is_mrnav', false);
        $isAJAX = $isAJAX && Mage::app()->getRequest()->isXmlHttpRequest();
        if (!$isAJAX)
            return;

        $layout->removeOutputBlock('root');
        $container = $layout->createBlock('core/template', 'mrnav_container');

        //product list
        $productList = Mage::helper('mr_nav')->findProductList($layout);
        if (!$productList) {
            return;
        }
        $container->setData('products', $productList->toHtml());

        //layer view
        $layerView = Mage::helper('mr_nav')->findLayerView($layout);
        if (!$layerView) {
            return;
        }
        $container->setData('layer', $layerView->toHtml());

        $layout->addOutputBlock('mrnav_container', 'toJson');
    }
}