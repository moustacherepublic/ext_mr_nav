<?php
class MR_Nav_Model_Catalog_Layer_Filter_Price extends Mage_Catalog_Model_Layer_Filter_Price
{
    /**
     * Minimal possible price
     */
    const MIN_POSSIBLE_PRICE = .01;

    /**
     * Apply price range filter
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param $filterBlock
     *
     * @return Mage_Catalog_Model_Layer_Filter_Price
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $currentVals = Mage::helper('mr_nav')->getRequestValues($this->_requestVar);

        if(empty($currentVals)){
            return $this;
        }

        $filters = $currentVals;
        $validFilters = array();
        foreach($filters as $filter){
            if($validFilter = $this->_validateFilter($filter)){
                $validFilters[] = $validFilter;
            }
        }

        $this->_applyPriceRanges($validFilters);
//        echo $this->getLayer()->getProductCollection()->getSelect();die;
        return $this;
    }

    protected function _applyPriceRanges($filters)
    {
        $finalConditions = array();
        foreach($filters as $filter){
            list($from, $to) = $filter;
            if ($from === '' && $to === '') {
                continue;
            }

            $select = $this->getLayer()->getProductCollection()->getSelect();
            $priceExpr = $this->_getPriceExpression($this, $select, false);

            if ($to !== '') {
                $to = (float)$to;
                if ($from == $to) {
                    $to += self::MIN_POSSIBLE_PRICE;
                }
            }

            $conditions = array();

            if ($from !== '') {
                $conditions[] = $priceExpr . ' >= ' . $this->_getComparingValue($from, $this);
            }
            if ($to !== '') {
                $conditions[] = $priceExpr . ' < ' . $this->_getComparingValue($to, $this);
            }

            $finalConditions []= '(' . implode(' AND ', $conditions) . ')';
        }

        if(!empty($finalConditions)){
            $select->where(implode(' OR ', $finalConditions));
        }

        return $this;
    }

    //copy from Mage_Catalog_Model_Resource_Layer_Filter_Price
    protected function _getPriceExpression($filter, $select, $replaceAlias = true)
    {
        $priceExpression = $filter->getLayer()->getProductCollection()->getPriceExpression($select);
        $additionalPriceExpression = $filter->getLayer()->getProductCollection()->getAdditionalPriceExpression($select);
        $result = empty($additionalPriceExpression)
            ? $priceExpression
            : "({$priceExpression} {$additionalPriceExpression})";
        if ($replaceAlias) {
            $result = $this->_replaceTableAlias($result);
        }

        return $result;
    }

    //copy from Mage_Catalog_Model_Resource_Layer_Filter_Price
    protected function _getComparingValue($price, $filter, $decrease = true)
    {
        $currencyRate = $filter->getLayer()->getProductCollection()->getCurrencyRate();
        if ($decrease) {
            return ($price - (self::MIN_POSSIBLE_PRICE / 2)) / $currencyRate;
        }
        return ($price + (self::MIN_POSSIBLE_PRICE / 2)) / $currencyRate;
    }

    /**
     * Get maximum price from layer products set
     *
     * @return float
     */
    public function getMaxPriceInt()
    {
        $maxPrice = $this->getData('max_price_int');
        if (is_null($maxPrice)) {
            $collection = $this->getLayer()->getCurrentCategory()->getProductCollection();
            $this->getLayer()->prepareProductCollection($collection);
            $maxPrice = $collection->getMaxPrice();
            $maxPrice = floor($maxPrice);
            $this->setData('max_price_int', $maxPrice);
        }

        return $maxPrice;
    }

    /**
     * Get data for build price filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
//        if (Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_CALCULATION) == self::RANGE_CALCULATION_IMPROVED) {
//            return $this->_getCalculatedItemsData();
//        } elseif ($this->getInterval()) {
//            return array();
//        }

        $range      = $this->getPriceRange();
        $dbRanges   = $this->getRangeItemCounts($range);
        $data       = array();
        $categoryDbRanges = $this->getCategoryRangeItemCounts($range);
        foreach($categoryDbRanges as $key => $count){
            if(array_key_exists($key, $dbRanges)){
                $categoryDbRanges[$key] = $dbRanges[$key];
            }
            else{
                $categoryDbRanges[$key] = '0';
            }
        }

//        var_dump($dbRanges, $categoryDbRanges);
        if (!empty($categoryDbRanges)) {
            $currentVals = Mage::helper('mr_nav')->getRequestValues($this->_requestVar);
            $lastIndex = array_keys($categoryDbRanges);
            $lastIndex = $lastIndex[count($lastIndex) - 1];
            foreach ($categoryDbRanges as $index => $count) {
                $fromPrice = ($index == 1) ? '' : (($index - 1) * $range);
                $toPrice = ($index == $lastIndex) ? '' : ($index * $range);
                $value = $fromPrice . '-' . $toPrice;

                $finalVals = $currentVals;
                $ind = array_search($value, $finalVals);
                if (false === $ind){
                    $finalVals[] = $value;
                }
                else {
                    $finalVals[$ind]  = null;
                    unset($finalVals[$ind]);
                }
                $value = implode(',', $finalVals);
                $selected = (false === $ind) ? false : true;
                $data[] = array(
                    'label' => $this->_renderRangeLabel($fromPrice, $toPrice),
                    'value' => $value,
                    'count' => $count,
                    'is_selected' => $selected,
                );
            }
        }

        return $data;
    }

    protected function _initItems()
    {
        $data  = $this->_getItemsData();
        $items = array();
        foreach ($data as $itemData) {
            $item = $this->_createItem(
                $itemData['label'],
                $itemData['value'],
                $itemData['count']
            );
            $item->setIsSelected($itemData['is_selected']);
            $items[] = clone $item;
        }
        $this->_items = $items;
        return $this;
    }

    /**
     * Get information about products count in range
     *
     * @param   int $range
     * @return  int
     */
    public function getCategoryRangeItemCounts($range)
    {
//        $rangeKey = 'range_item_counts_' . $range;
//        $items = $this->getData($rangeKey);
//        if (is_null($items)) {
            $items = $this->_getResource()->getCategoryCount($this, $range);//modified
            // checking max number of intervals
            $i = 0;
            $lastIndex = null;
            $maxIntervalsNumber = $this->getMaxIntervalsNumber();
            $calculation = Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_CALCULATION);
            foreach ($items as $k => $v) {
                ++$i;
                if ($calculation == self::RANGE_CALCULATION_MANUAL && $i > 1 && $i > $maxIntervalsNumber) {
                    $items[$lastIndex] += $v;
                    unset($items[$k]);
                } else {
                    $lastIndex = $k;
                }
            }
//            $this->setData($rangeKey, $items);
//        }

        return $items;
    }

    public function getPriceRange()
    {
        $range = $this->getData('price_range');
        if (!$range) {
            $currentCategory = Mage::registry('current_category_filter');
            if ($currentCategory) {
                $range = $currentCategory->getFilterPriceRange();
            } else {
                $range = $this->getLayer()->getCurrentCategory()->getFilterPriceRange();
            }

            $maxPrice = $this->getMaxPriceInt();
            if (!$range) {
                $calculation = Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_CALCULATION);
                if ($calculation == self::RANGE_CALCULATION_AUTO) {
                    $index = 1;
//                    do {
                        $range = pow(10, (strlen(floor($maxPrice)) - $index));
                        $items = $this->getRangeItemCounts($range);
                        $index++;
//                    }
//                    while($range > self::MIN_RANGE_POWER );
                } else {
                    $range = (float)Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_STEP);
                }
            }

            $this->setData('price_range', $range);
        }

        return $range;
    }
}