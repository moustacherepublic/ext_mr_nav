<?php
class MR_Nav_Model_Catalog_Layer_Filter_Category extends Mage_Catalog_Model_Layer_Filter_Category {
    /**
     * Apply category filter to layer
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Mage_Core_Block_Abstract $filterBlock
     * @return  Mage_Catalog_Model_Layer_Filter_Category
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $currentVals = Mage::helper('mr_nav')->getRequestValues($this->_requestVar);
        if($currentVals){
            $this->getLayer()->getProductCollection()
                ->joinField('category_id', 'catalog/category_product', 'category_id',
                    'product_id = entity_id', null, 'left')
                ->addAttributeToFilter('category_id', array('in' => $currentVals));
            //avoid duplicate items which exist in more than 2 categories
            $this->getLayer()->getProductCollection()->getSelect()->group('e.entity_id');
        }

        return $this;
    }

    public function getCategory()
    {
        return $this->getLayer()->getCurrentCategory();
    }

    /**
     * Get data array for building category filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
        $category   = $this->getCategory();
        $categories = $category->getChildrenCategories();
        $activeFilters = $this->getLayer()->getState()->getFilters();
        if(empty($activeFilters)){
            $collection = $this->getCategory()->getProductCollection();
            $this->getLayer()->prepareProductCollection($collection);
            $collection->addCountToCategories($categories);
        }
        else{
            $this->getLayer()->getProductCollection()
                ->addCountToCategories($categories);
        }

        $data = array();
        foreach ($categories as $category) {
            if ($category->getIsActive() && $category->getProductCount()) {
                $currentVals = Mage::helper('mr_nav')->getRequestValues($this->_requestVar);
                $ind = array_search($category->getId(), $currentVals);
                if (false === $ind){
                    $currentVals[] = $category->getId();
                }
                else {
                    $currentVals[$ind]  = null;
                    unset($currentVals[$ind]);
                }

                $currentVals = implode(',', $currentVals);
                $selected = (false === $ind) ? false : true;
                $data[] = array(
                    'label' => Mage::helper('core')->escapeHtml($category->getName()),
                    'value' => $currentVals,
                    'count' => $category->getProductCount(),
                    'is_selected' => $selected,
                );
            }
        }
        return $data;
    }

    protected function _initItems()
    {
        $data  = $this->_getItemsData();
        $items = array();
        foreach ($data as $itemData) {
            $item = $this->_createItem(
                $itemData['label'],
                $itemData['value'],
                $itemData['count']
            );
            $item->setIsSelected($itemData['is_selected']);
            $items[] = clone $item;
        }
        $this->_items = $items;
        return $this;
    }
}