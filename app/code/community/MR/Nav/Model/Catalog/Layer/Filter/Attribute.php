<?php
class MR_Nav_Model_Catalog_Layer_Filter_Attribute extends Mage_Catalog_Model_Layer_Filter_Attribute
{
    
    protected function applyFilterToCollection($value){
        $collection = $this->getLayer()->getProductCollection();
        $attribute  = $this->getAttributeModel();
        $alias      = $this->getAttributeModel()->getAttributeCode() . '_idx';
        $connection = $this->_getResource()->getReadConnection();
        $conditions = array(
            "{$alias}.entity_id = e.entity_id",
            $connection->quoteInto("{$alias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$alias}.store_id = ?",     $collection->getStoreId()),
            $connection->quoteInto("{$alias}.value IN(?)",      $value)
        );
	            
        $collection->getSelect()->join(
            array($alias => $this->_getResource()->getMainTable()),
            join(' AND ', $conditions),
            array()
        );
        
        if(count($value) > 1){
            $collection->getSelect()->distinct(true);
        }
        
        return $this;
    }
    
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock) {
        $currentVals = Mage::helper('mr_nav')->getRequestValues($this->_requestVar);
        if($currentVals){
            $this->applyFilterToCollection($currentVals);
            
            $attribute = $this->getAttributeModel();    
            $text = '';
            foreach ($attribute->getSource()->getAllOptions() as $option) {
                $k = array_search($option['value'], $currentVals);
                if (false !== $k){
                    $exclude = $currentVals;
                    unset($exclude[$k]);
                    $exclude = implode(',', $exclude);
                    if (!$exclude)
                        $exclude = null;
                    
                    $query = array(
                        $this->_requestVar => $exclude,
                        Mage::getBlockSingleton('page/html_pager')->getPageVarName() => null // exclude current page from urls
                    );
                    
                    $url = Mage::getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true, '_query'=>$query));                
                    $text .= $option['label']
                          . '&nbsp;'
                          . '<a href="' . $url . '" class="btn-remove">'
//                          . '<img src="' . $this->_getRemoveImage() . '" alt="' . Mage::helper('catalog')->__('Remove This Item') . '" />'
                          . '</a><br />';
                }
            }
            
            $this->getLayer()->getState()->addFilter($this->_createItem($text, $currentVals));
        }
        
        return $this;
    }
    
    protected function _getCount($attribute)
    {
        $optionsCount = array();

        // clone select from collection with filters
        $select = $this->_getBaseCollectionSql();

        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);

        $connection = $this->_getResource()->getReadConnection();
        $tableAlias = $attribute->getAttributeCode() . '_idx';
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $this->getStoreId()),
        );

        $select
            ->join(
                array($tableAlias => $this->_getResource()->getMainTable()),
                join(' AND ', $conditions),
                array('value', 'count' => "COUNT(DISTINCT {$tableAlias}.entity_id)"))
            ->group("{$tableAlias}.value");

        $optionsCount = $connection->fetchPairs($select);
         
         
        return $optionsCount;       
    }
    
    protected function _getBaseCollectionSql()
    {
        $alias = $this->getAttributeModel()->getAttributeCode() . '_idx';
        
        $baseSelect = clone parent::_getBaseCollectionSql();
        
        $oldWhere = $baseSelect->getPart(Varien_Db_Select::WHERE);
        $newWhere = array();

        foreach ($oldWhere as $cond){
           if (!strpos($cond, $alias))
               $newWhere[] = $cond;
        }
  
        if ($newWhere && substr($newWhere[0], 0, 3) == 'AND')
           $newWhere[0] = substr($newWhere[0], 3);        
        
        $baseSelect->setPart(Varien_Db_Select::WHERE, $newWhere);
        
        $oldFrom = $baseSelect->getPart(Varien_Db_Select::FROM);
        $newFrom = array();
        
        foreach ($oldFrom as $name=>$val){
           if ($name != $alias)
               $newFrom[$name] = $val;
        }
        $baseSelect->setPart(Varien_Db_Select::FROM, $newFrom);

        return $baseSelect;
    }
    
    protected function _getItemsData()
    {
        $attribute = $this->getAttributeModel();
        $this->_requestVar = $attribute->getAttributeCode();


        $options = $attribute->getFrontend()->getSelectOptions();
        $optionsCount = $this->_getCount($attribute);
        $data = array();

        foreach ($options as $option) {
            if (is_array($option['value'])) {
                continue;
            }
            if (!Mage::helper('core/string')->strlen($option['value'])) {
                continue;
            }
            $currentVals = Mage::helper('mr_nav')->getRequestValues($this->_requestVar);
            $ind = array_search($option['value'], $currentVals);
            if (false === $ind){
                $currentVals[] = $option['value'];
            }
            else {
                $currentVals[$ind]  = null;
                unset($currentVals[$ind]);    
            }
            
            $currentVals = implode(',', $currentVals);
            $cnt = isset($optionsCount[$option['value']]) ? $optionsCount[$option['value']] : 0; 
            $selected = (false === $ind) ? false : true;
            if ($cnt || $this->_getIsFilterableAttribute($attribute) != self::OPTIONS_ONLY_WITH_RESULTS) {
                    $data[] = array(
                        'label'     => $option['label'],
                        'value'     => $currentVals,
                        'count'     => $cnt,
                        'option_id' => $option['value'],
                        'is_selected' => $selected,
                    );
            }
        }
        return $data;
    }
    
    protected function _getRemoveImage()
    {
        return Mage::getDesign()->getSkinUrl('images/btn_remove2.gif');            
    }
    
    protected function _initItems()
    {
        $data  = $this->_getItemsData();
        $items = array();
        foreach ($data as $itemData) {
            $item = $this->_createItem(
                $itemData['label'],
                $itemData['value'],
                $itemData['count']
            );
            $item->setOptionId($itemData['option_id']);
            $item->setIsSelected($itemData['is_selected']);
            $items[] = clone $item;
        }
        $this->_items = $items;
        return $this;
    } 
}
?>
